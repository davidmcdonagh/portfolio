from code.cocrystal_stabilisation.single_component import SingleComponent

class Cocrystal:
    
    """
    Class containing pairs of SingleComponent instances for a
    1:1 cocrystal, and the corresponding stabilisation energy.
            
    """

    def __init__(self, ccdc_ref, single_components, stabilisation_energy=None):
        
        """
        Parameters
        ----------
        ccdc_ref : string
            CCDC ref code if available (not_observed if not).
        single_components : list of SingleComponent
            Two SingleComponent instances.
        stabilisation_energy : float
            The stabilisation energy of the cocrystral, compared
            to the two single components.
        """
        
        assert len(single_components) == 2, \
        "single_components must be a list of length 2"
        assert isinstance(single_components[0], SingleComponent), \
        "single_components must be SingleComponent instances"
        assert isinstance(single_components[1], SingleComponent), \
        "single_components must be SingleComponent instances"
        assert isinstance(ccdc_ref, str), \
        "ccdc_ref must be a string"
        assert isinstance(stabilisation_energy, float) or isinstance(stabilisation, int),\
        "stabilisation_energy must be a float/int"

        self.single_components = single_components
        self.ccdc_ref = ccdc_ref
        self.stabilisation_energy = stabilisation_energy
