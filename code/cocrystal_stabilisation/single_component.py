from rdkit import Chem
from rdkit.Chem import Descriptors

class SingleComponent:
    
    """
    Class containing descriptors generated from
    smiles strings.
    """

    def __init__(self, name, smiles_string, ccdc_ref):
        
        """
        Parameters
        ----------
        name : string
            The name of the molecule.
        smules_string : string
            The smiles string representation.
        ccdc_ref : string
            A CCDC refcode of a crystal structure
            containing this molecule.
            
        Notes
        -----
        For molecules with multipole names, names
        were selected (arbitrarily)from those in the CCDC. The purpose
        of the name is to make it easy to look
        for structures containing these molecules, rather than to follow
        a strict convention.
        For polymorphic molecules, a ccdc_ref is again chosen
        arbitrarily.
        """
        
        assert isinstance(name, str), "name must be a string"
        assert isinstance(smiles_string, str), "smiles_string must be a string"
        assert isinstance(ccdc_ref, str), "ccdc_ref must be a string"

        self.name = name
        self.smiles_string = smiles_string
        self.ccdc_ref = ccdc_ref
        self.descriptors = {}
        self.calc_all_rdkit_descriptors()
        
    def calc_all_rdkit_descriptors(self):
        
        """
        Iterates over all molecule descriptors in rdkit
        and passes them to a dictionary.
        
        Updates
        -------
        self.descriptors
        """
        
        mol = Chem.MolFromSmiles(self.smiles_string)
        mol = Chem.AddHs(mol)
        for count,i in enumerate(Descriptors._descList):
            self.descriptors[Descriptors._descList[count][0]] = \
            Descriptors._descList[count][1](mol)
