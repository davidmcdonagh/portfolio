from code.cocrystal_stabilisation.cocrystal import Cocrystal
from code.cocrystal_stabilisation.single_component import SingleComponent

class CocrystalCollection:
    
    """
    Class containing a list of Cocrystal instances,
    and functions to extract data from them.
    """
    
    def __init__(self, cocrystals):
        
        """
        Parameters
        ----------
        cocrystals : list of Cocrystal
        
        """
        
        self.collection = cocrystals
        
    def __len__(self):
        return len(self.collection)

    def __getitem__(self, key):
        if isinstance(key, int):
            return self.collection[key]
        elif isinstance(key, basestring):
            for i in self.collection:
                if i.name == key:
                    return i
            print ("{} not in collection.".format(key))
            return None
        
    def get_cocrystals_with_single_component(self, name):
        
        """
        Returns a list of all cocrystals in self.collection
        containing single components called name.
        
        Parameters
        ----------
        name : string
        
        Returns
        -------
        list of Cocrystal
        """
        return [i for i in self.collection if name \
                in [j.name for j in i.single_components]]
    
    
    def get_stacked_descriptors(self, descriptor_names=None):
    
        """
        Concatenates all descriptors for both single components
        for every Cocrystal. Outputs list of concatenated descriptors,
        and a corresponding list of stabilisation energies.
        
        
        Parameters
        ----------
        descriptor_names : list of strings, optional
            If not None, only descriptors with names in
            descriptor_names are included.
            
        Returns
        -------
        descriptors : 2D numpy array
        stabilisation_energies : list of floats
        
        Notes
        -----
        Single components are concatenated in both configurations,
        and so the returned lists will contain two entries for each Cocrystal.
        """
        
        descriptors = []
        stabilisation_energies = []
        
        for cc in self.collection:
            if descriptor_names is None:
                cc1 = list(cc.single_components[0].descriptors.values()) \
                        + list(cc.single_components[1].descriptors.values())
                cc2 = list(cc.single_components[1].descriptors.values()) \
                        + list(cc.single_components[0].descriptors.values())     
            else:
                sc1_d = [cc.single_components[0].descriptors[j] \
                         for j in descriptor_names]
                sc2_d = [cc.single_components[1].descriptors[j] \
                         for j in descriptor_names]
                
                cc1 = sc1_d + sc2_d
                cc2 = sc2_d + sc1_d
            
            descriptors.append(cc1)
            stabilisation_energies.append(cc.stabilisation_energy)
            descriptors.append(cc2)
            stabilisation_energies.append(cc.stabilisation_energy)
            
        return np.array([np.array(i) for i in descriptors]), stabilisation_energies
            

    def get_descriptor_diffs(self, descriptor_names=None):
        
        """
        Returns the absolute difference of all descriptors for each
        cocrystal. Outputs list of absolute descriptor differences, 
        and a corresponding list of stabilisation energies.
        
        Parameters
        ----------
        descriptor_names : list of strings, optional
            If not None, only descriptors with names in
            descriptor_names are included.
            
        Returns
        ------
        descriptors : 2D numpy array
        stabilisation_energies : list of floats
        """
        
        descriptors = []
        stabilisation_energies = []
        for cc in self.collection:
            if descriptor_names is None:
                sc1_d = list(cc.single_components[0].descriptors.values())
                sc2_d = list(cc.single_components[1].descriptors.values())
                assert len(sc1_d) == len(sc2_d), "single components must have "\
                "the same number of descriptors."
                descriptors.append([abs(sc1_d[i] - sc2_d[i]) for i in range(len(sc1_d))])
                stabilisation_energies.append(cc.stabilisation_energy)
            else:
                sc1_d = [cc.single_components[0].descriptors[j] \
                         for j in descriptor_names]
                sc2_d = [cc.single_components[1].descriptors[j] \
                         for j in descriptor_names]
                assert len(sc1_d) == len(sc2_d), "single components must have "\
                "the same number of descriptors."
                
                cc_descriptors = [abs(sc1_d[i] - sc2_d[i]) for i in range(len(sc1_d))]
                descriptors.append(cc_descriptors)
                stabilisation_energies.append(cc.stabilisation_energy)
                
        return np.array([np.array(i) for i in descriptors]), stabilisation_energies
                
