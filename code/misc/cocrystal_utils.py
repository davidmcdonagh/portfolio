import csv
from rdkit.Chem import MolFromSmiles, AddHs
from rdkit.Chem.AllChem import EmbedMultipleConfs, UFFOptimizeMoleculeConfs
from rdkit.ML.Cluster import Butina
from rdkit.Chem.AllChem import GetConformerRMSMatrix

def get_single_components_from_csv(csv_file):
    
    """
    Reads a .csv file and creates a list of SingleComponent
    instances.
    
    Parameters
    ----------
    csv_file : string
        Path to .csv file
        
    Returns
    -------
    single_components : list of SingleComponent
    
    Notes
    -----
    The .csv file is assumed to have the format
    name1, smiles_string1, ccdc_ref1
    name2, smiles_string2, ccdc_ref2
    etc.
    
    """
    single_components = []
    with open(csv_file, newline='') as csv_f:
        csv_reader = csv.reader(csv_f, delimiter=',')
        for row in csv_reader:
            single_components.append(SingleComponent(row[0], row[1], row[2]))
        return single_components

def get_cocrystals_from_csv(csv_file, single_components_list):
    
    """
    Reads a .csv file and creates a list of Cocrystal instances.
    
    Parameters
    ----------
    csv_file : string
        Path to a .csv file
        
    Returns
    -------
    cocrystals : list of Cocrystal
    
    Notes
    -----
    The .csv file is assumed to have the row format
    ccdc_ref, single_component_name1, single_component_name2, 
    stabilisation_energy
    If the Cocrystal has not been observed, the name is assumed 
    to be not_observed.

    """
    
    cocrystals = []
    sc_dict = {i.name : i for i in single_components_list}
    with open(csv_file, newline='') as csv_f:
        csv_reader = csv.reader(csv_f, delimiter=',')
        for row in csv_reader:
            ccdc_ref = row[0]
            sc_name1 = row[1]
            sc_name2 = row[2]
            single_components = [sc_dict[sc_name1], sc_dict[sc_name2]]
            stabilisation_energy = float(row[3])
            cocrystals.append(Cocrystal(ccdc_ref, single_components,\
                              stabilisation_energy))
    return cocrystals


def gen_molecule_com_file_from_smiles_string(smiles_string, name, 
                                            num_conformers=1000, method="B3LYP",
                                            basis_set="6-311G**", charge="0", 
                                            multiplicity="1"):

    """
    Uses rdkit to find the lowest energy conformer of a smiles string.

    Parameters
    ----------
    smiles_string : string
        Smiles string of the molecule.
    name : string
        Name of the molecule.
    num_conformers : int, optional
        The number of intial conformers generated.
    method : string, optional,
        The electronic structure method entered in the com file output.
    basis_set : string, optional,
        The basis set entered in the com file output.
    charge : string, optional
        The charge of the molecule.
    multiplicity : string, optional
        The multiplicity of the molecule.
    
    
    Returns
    -------
    g09_file : string
        A Gaussian09 com file for final optimisation.

    """

    assert isinstance(smiles_string, str), "smiles_string must be a string"

    conformer, mol = get_low_energy_conformer(smiles_string, num_conformers)
    atom_data = get_conformer_atom_data(conformer, mol)
    return gen_g09_input(atom_data,
           name=name, method=method, basis_set=basis_set, charge=charge, multiplicity=multiplicity)


def gen_conformer_com_files(smiles_string, energy_threshold=10.0, num_conformers=1000, 
                                           cluster_threshold=1.0, method="B3LYP",
                                            basis_set="6-311G**", charge="0", multiplicity="1"):

    """
    Uses rdkit to find the lowest energy conformers within threshold of lowest energy conformer,
    Writes a .com Gaussian 09 file for each unique conformer.

    Parameters
    ----------
    smiles_string : string
        Smiles string of the molecule.
    energy_threshold : float, optional
        The energy in kJ/mol above the global minimum where conformers
        will be retained.
    num_conformers : int, optional
        The number of intial conformers generated.
    cluster_threshold : float, optional
        The RMSD threshold below which two conformers are considered the same.
    method : string, optional,
        The electronic structure method entered in the com file output.
    basis_set : string, optional,
        The basis set entered in the com file output.
    charge : string, optional
        The charge of the molecule.
    multiplicity : string, optional
        The multiplicity of the molecule.
    
    
    Output
    -------
    Gaussian 09 com files for each unique conformer found.

    """

    assert isinstance(smiles_string, str), "smiles_string must be a string"

    opted_conf_ids, mol = get_conformer_list(smiles_string, num_conformers)
    cluster_conf_ids = cluster_conformers(mol, opted_conf_ids, cluster_threshold)

    base_name = "conf"
    min_e = cluster_conf_ids[0][1]
    threshold = min_e + energy_threshold
    for count, i in enumerate(cluster_conf_ids):
        if i[1] > threshold:
            break
        name = base_name + str(count)
        conf =  mol.GetConformer(i[0])
        atom_data = get_conformer_atom_data(conf, mol)
        with open(name + ".com", "w") as g:
            g.write(gen_g09_input(atom_data,
            name=name, method=method, basis_set=basis_set, charge=charge, multiplicity=multiplicity))

def get_low_energy_conformer(smiles_string, num_conformers):

    """
    Generates num_conformers based on smiles_string
    Conformers are optimised using the rdkit universal force field (UFF).

    Parameters
    ----------
    smiles_string : string
        Smiles string of the molecule.
    num_conformers : int
        Number of initial conformers to generate.

    Returns
    -------
    conf : Conformer
        The lowest energy conformer.
    mol : Mol
        The mol object that the conformer belongs to.
    """

    # Find lowest energy conformer
    mol = MolFromSmiles(smiles_string)
    mol = AddHs(mol)
    EmbedMultipleConfs(mol, numConfs=num_conformers)
    opted_confs = UFFOptimizeMoleculeConfs(mol)
    sorted_opted_confs = sorted([[i[0],i[1][1]] for i in enumerate(opted_confs)], key=lambda x:x[1])
    energy_range = sorted_opted_confs[-1][1] - sorted_opted_confs[0][1]
    print("Energy range from minimum:", energy_range, "kJ/mol")
    lowest_conf_id = sorted_opted_confs[0][0]
    conf =  mol.GetConformer(lowest_conf_id)
    return conf, mol


def get_conformer_list(smiles_string, num_conformers):

    """
    Using the same methods as get_low_energy_conformer,
    but returns a list of optimised conformers.
   
    Parameters                                      
    ----------
    smiles_string : string
        Smiles string of the molecule.
    num_conformers : int
        Number of initial conformers to generate.

    Returns
    -------
    opted_conf_ids : list [[conf_id, energy]]
    mol : Mol
        The mol object holding the conformer data.

    """

    # Find lowest energy conformer
    mol = MolFromSmiles(smiles_string)
    mol = AddHs(mol)
    EmbedMultipleConfs(mol, numConfs=num_conformers)
    opted_confs = UFFOptimizeMoleculeConfs(mol)
    opted_conf_ids = [[i[0],i[1][1]] for i in enumerate(opted_confs)]
    return opted_conf_ids, mol

def cluster_conformers(mol, opted_conf_ids, threshold=1.0):

    """
    Clusters conformers based on RMSD.

    Parameters
    ----------
    mol : Mol
        Mol object holding conformer data
    opted_conf_ids : list [[conf_id, energy]]
    threshold : float, optional
        RMSD threshold below which two conformers are considered the same.

    Returns
    -------
    final_conf_ids : list of ints

    """


    dmat = GetConformerRMSMatrix(mol, prealigned=False)
    rms_clusters = Butina.ClusterData(dmat, mol.GetNumConformers(), threshold, isDistData=True, reordering=True)
    conf_clusters = [[] for i in range(len(rms_clusters))]
    for i in opted_conf_ids:
        for count_j, j in enumerate(rms_clusters):
            if i[0] in j:
                conf_clusters[count_j].append(i)
                break

    for i in range(len(conf_clusters)):
        conf_clusters[i] = sorted(conf_clusters[i], key=lambda x:x[1])
    final_conf_ids = sorted([i[0] for i in conf_clusters], key=lambda x:x[1])
    return final_conf_ids

def get_conformer_atom_data(conformer, mol):

    """
    Extracts xyz and atomic number data from conformer instance
    Returns string of data
    """

    atom_data = ""
    atom_types = [str(i.GetAtomicNum()) for i in mol.GetAtoms()]
    for i in range(conformer.GetNumAtoms()):
        pos = conformer.GetAtomPosition(i)
        atom_data+= atom_types[i] + " " +  str(pos.x) + " " +  str(pos.y) + " " +  str(pos.z) + "\n"

    return atom_data

def gen_g09_input(atom_data, name, method="B3LYP", basis_set="6-311G**", charge="0", multiplicity="1"):

    """
    Writes a Gaussian 09 script to geometry optimise a molecule from atom_data
    output=wfn is also added so that the calculation returns the .wfn file for calulating the MEPS
    Returns string form of Gaussian input (.com) file
    """

    g09_input = "%NProcShared=2\n%mem=3GB\n"
    g09_input += "#%s/%s NoSymmetry Opt output=wfn\n\n" % (method, basis_set)
    g09_input += "conformer geom opt \n\n"
    g09_input += "%s,%s\n" % (charge, multiplicity)
    g09_input += atom_data
    g09_input += "\n"
    g09_input += name + ".wfn\n"

    return g09_input

