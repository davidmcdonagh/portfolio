from os.path import isfile

def get_opt_xyz_from_log(log_file, number_atoms=False):

    """
    Finds the geometry optimised xyz in a .log file.
    Currently only valid for molecules with H,C,N,O,Cl.

    Parameters
    ----------
    log_file : string
        Path to a Gaussian .log file.

    number_atoms : boolean, optional
        If True, atom labels will be numbered.

    Returns
    -------
    molecule_xyz : string
        The optimised molecular geometry in xyz format.

    """


    assert isinstance(log_file, str), "log_file must be a string."
    assert isfile(log_file), "Cannot find log_file at {}".format(log_file)

    # Dictionary to convert atomic numbers to labels
    atom_dict = {1 : 'H', 6 : 'C', 7 : 'N', 8 : 'O', 17 : 'Cl'}
    # Bookkeeping if number_atoms is True
    num_atoms = {'H' : 0, 'C' : 0, 'N' : 0, 'O' : 0, 'Cl' : 0}

    with open(log_file, "r") as g:
        lines = g.readlines()

    # Check for normal termination
    if not lines[-1].startswith(" Normal termination"):
        print("G09 calculation did not terminate normally")
        print("End of file:\n")
        print(lines[-10:])
        raise AssertionError

    opt_finished=False
    extrating_molecule_xyz=False
    molecule_xyz=[]
    for count, line in enumerate(lines):
        if line.startswith(" Optimization completed."):
            opt_finished=True

        # End of molecule xyz
        if line.startswith(" ----------") and extrating_molecule_xyz:
            molecule_xyz = str(len(molecule_xyz)) \
            + "\n" + "comment line\n" + "\n".join(molecule_xyz)
            return molecule_xyz

        if extrating_molecule_xyz:
            ls = line.split()
            atomic_number = ls[1]
            atom_label = atom_dict[int(atomic_number)]
            num_atoms[atom_label] += 1
            if number_atoms:
                full_atom_label = [atom_label + str(num_atoms[atom_label])]
            else:
                full_atom_label = [atom_label]
            atom_xyz = ls[3:]
            molecule_xyz.append("       ".join(full_atom_label  + atom_xyz))

        # Start of molecule xyz
        if line.startswith(" ----------") \
        and opt_finished \
        and lines[count-1].startswith(" Number     Number       Type"):
            extrating_molecule_xyz=True

def get_scf_energy(log_file, includes_counterpoise=False):

    """
    Extracts the energy from a gaussian log file.

    Parameters
    ----------
    log_file : string
        Path to a .log file.
    includes_counterpoise : boolean, optional
        If True, the counterpoise corrected energy is instead returned.

    Returns
    -------
    energy : float

    """ 

    assert isinstance(log_file, str), "log_file must be a string."
    assert isfile(log_file), "Cannot find log_file at {}".format(log_file)

    with open(log_file, "r") as g:
        lines = g.readlines()         

    assert lines[-1].startswith(" Normal termination"), \
           "Log file did not terminate normally."

    energy=None

    if not includes_counterpoise:

        for line in lines:
            if line.startswith(" SCF Done:"):
                energy = line.split()[4]

        assert energy is not None, "Could not find energy."
        return float(energy)
    else:
        for line in lines:
            if line.startswith(" Counterpoise corrected energy ="):
                job_finished = True
                absolute_energy = float(line.split()[4])
            if line.startswith("               sum of monomers ="):
                sum_monomers = float(line.split()[4])
                return absolute_energy - sum_monomers

        raise IndexError("Could not fnid energy")



















