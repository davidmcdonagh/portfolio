import unittest
import numpy as np

class GaussianUtils(unittest.TestCase):

    def test_get_scf_energy(self):
        from code.misc.gaussian_utils import get_scf_energy
        log_file = "test_files/gaussian_opt_test.log"
        correct_energy = -799.928065777
        energy = get_scf_energy(log_file)
        self.assertAlmostEqual(correct_energy, energy)

    def test_get_opt_xyz_from_log(self):
        from code.misc.gaussian_utils import get_opt_xyz_from_log
        log_file = "test_files/gaussian_opt_test.log"
        with open("test_files/gaussian_opt_test.xyz", "r") as g:
            expected_output1 = g.read()[:-2]

        xyz1 = get_opt_xyz_from_log(log_file)
        # check number of atoms
        self.assertEqual(int(xyz1[:2]), int(expected_output1[:2]))
        # check first xyz line
        xyz1_line = xyz1.split("\n")[2].split()
        eo1_line = expected_output1.split("\n")[2].split()
        self.assertEqual(xyz1_line[0], eo1_line[0])
        self.assertAlmostEqual(float(xyz1_line[1]), float(eo1_line[1]))
        self.assertAlmostEqual(float(xyz1_line[2]), float(eo1_line[2]))
        self.assertAlmostEqual(float(xyz1_line[3]), float(eo1_line[3]))

        with open("test_files/gaussian_opt_test_number_atoms.xyz", "r") as g:
            expected_output2 = g.read()[:-2]

        xyz2 = get_opt_xyz_from_log(log_file, number_atoms=True)
        # check number of atoms
        self.assertEqual(int(xyz2[:2]), int(expected_output2[:2]))
        # check first xyz line
        xyz2_line = xyz2.split("\n")[2].split()
        eo2_line = expected_output2.split("\n")[2].split()
        self.assertEqual(xyz2_line[0], eo2_line[0])
        self.assertAlmostEqual(float(xyz2_line[1]), float(eo2_line[1]))
        self.assertAlmostEqual(float(xyz2_line[2]), float(eo2_line[2]))
        self.assertAlmostEqual(float(xyz2_line[3]), float(eo2_line[3]))


class MlUtils(unittest.TestCase):

    def test_get_mean_squared_error(self):
        from code.misc.ml_utils import get_mean_squared_error
        dummy_data1 = [1,5,2,6]
        dummy_data2 = [3,9,2,8]
        known_answer = 6.0
        self.assertAlmostEqual(known_answer, \
                               get_mean_squared_error(dummy_data1, dummy_data2))


    def test_get_rmse(self):
        from code.misc.ml_utils import get_rmse
        dummy_data1 = [1,5,2,6]
        dummy_data2 = [3,9,2,8]
        known_answer = 2.449489742783178
        self.assertAlmostEqual(known_answer, \
                               get_rmse(dummy_data1, dummy_data2))


    def test_get_mean_signed_error(self):
        from code.misc.ml_utils import get_mean_signed_error
        dummy_data1 = [1,5,2,6]
        dummy_data2 = [3,9,2,8]
        known_answer = -8
        self.assertAlmostEqual(known_answer, \
                               get_mean_signed_error(dummy_data1, dummy_data2))

 

class DataHandling(unittest.TestCase):

    def test_scale_data(self):
        from code.misc.data_handling import scale_data
        dummy_data1 = np.array(([0,3,1], [9,2,1]))
        dummy_data2 = np.array(([5,3,2], [3,3,1]))
        x1, x2 = scale_data(dummy_data1, dummy_data2)
        known_answer1 = np.array(([-1., 1., 0.], [1., -1., 0.]))
        known_answer2 = np.array(([ 0.11111111,1.,1.],[-0.33333333,1.,0.]))
        self.assertTrue(np.allclose(x1, known_answer1))
        self.assertTrue(np.allclose(x2, known_answer2))


    def test_remove_zeros(self):
        from code.misc.data_handling import remove_zeros
        dummy_data = np.array(([0,2,2], [0,1,0]))
        expected_result = np.array(([2,2],[1,0])) 
        result = remove_zeros(dummy_data)
        self.assertTrue(np.allclose(result, expected_result))

    def test_remove_correlated(self):
        from code.misc.data_handling import remove_correlated
        dummy_data = np.array(([1,84,1], [3,-30,3]))
        expected_result = np.array(([1,84],[3,-30]))
        result = remove_correlated(dummy_data, threshold=0.9)
        self.assertTrue(np.allclose(result, expected_result))       

    def test_get_max_min_idxs(self):
        from code.misc.data_handling import get_max_min_idxs
        dummy_data = np.array(([1,1,1], [1,0,1], [98,2,50]))
        expected_result = [1,2]
        result = get_max_min_idxs(dummy_data, tf=.5)
        self.assertTrue(np.allclose(result, expected_result))

    def test_split_data_by_max_min(self):
        from code.misc.data_handling import split_data_by_max_min
        dummy_x = np.array(([1,1,1], [1,0,1], [98,2,50]))
        dummy_y = np.array(([7,4,9]))
        expected_training_x = np.array(([1,0,1],[98,2,50]))
        expected_training_y = np.array(([4, 9]))
        expected_testing_x = np.array(([1,1,1]))
        expected_testing_y = np.array(([7]))
        training_x, training_y, testing_x, testing_y = split_data_by_max_min(dummy_x, dummy_y, tf=.5)
        self.assertTrue(np.allclose(expected_training_x, training_x))
        self.assertTrue(np.allclose(expected_training_y, training_y))
        self.assertTrue(np.allclose(expected_testing_x, testing_x))
        self.assertTrue(np.allclose(expected_testing_y, testing_y))
        
        











if __name__ == '__main__':
    unittest.main()

