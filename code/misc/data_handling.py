import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.metrics.pairwise import euclidean_distances
from math import ceil
import matplotlib.pyplot as plt

def scale_data(training_x, testing_x):

    """
    Returns scaled training_x, testing_x to give a 0 mean
    Scales testing_x based on training_x ranges.

    Parameters
    ----------
    training_x : 2D array
    testing_x : 2D array

    Returns
    -------
    scaled_training_x : 2D array
    scaled_testing_x : 2D array

    """

    scaler = StandardScaler()
    scaler.fit(training_x)
    return scaler.transform(training_x), scaler.transform(testing_x)



def remove_zeros(data, labels=None):

    """
    Returns data_list_2D with columns removed that are zero for all rows.
    Removes corresponding labels in label list if given.
     
    Parameters
    ----------
    data : 2D array
    labels : list of strings, optional

    Returns
    -------
    output_data_arr : 2D array
    labels : list of strings (if labels is not None) 
    """

    zero_idxs = np.where(~data.T.any(axis=1))[0]
    output_data_arr = data.T[~np.all(data == 0, axis=0)].T
    if labels is None:
        return output_data_arr
    labels = [labels[i] for i in range(len(labels)) if i not in zero_idxs]
    return output_data_arr, labels


def remove_correlated(data_list_2D, threshold, labels=None):

    """
    Returns data_list_2D with correlated columns removed.

    Parameters
    ----------
    data_list_2D : list of lists of floats
    threshold : float
        The correlation threshold above which they are considered
        correlated.
    labels : list of strings, optional
        If labels is not None, labels at the idxs of the removed columns
        are also returned.

    Returns
    -------
    uncorrelated_data : 2D array
    uncorrelated_labels : list of strings (if labels is not None)

    Notes
    -----
    If two columns have a correlation coefficient greater than threshold,
    only one of the columns is retained. The choice is order dependent.

    """
    
    def isCorrelated(x1, x2, threshold):
        return np.corrcoef(x1, x2)[0, 1] >= threshold
    
    uncorrelated_data = []
    if labels is not None:
        uncorrelated_labels = []
        remaining_labels = labels[:]
    remaining_data = np.copy(data_list_2D).T


    while len(remaining_data) > 1:
        uncorrelated_data.append(remaining_data[0])
        if labels is not None:
            uncorrelated_labels.append(remaining_labels[0])
            del remaining_labels[0]
        remaining_data = np.delete(remaining_data, (0),axis=0)
        del_idxs = []
        for count, i in enumerate(remaining_data):
            if isCorrelated(uncorrelated_data[-1], i, threshold):
                del_idxs.append(count)
        del_idxs = sorted(del_idxs, reverse=True)
        for i in del_idxs:
            remaining_data = np.delete(remaining_data, (i),axis=0)
            if labels is not None:
                del remaining_labels[i]
    if len(remaining_data) == 1:
        uncorrelated_data.append(remaining_data[0])
        if labels is not None:
            uncorrelated_labels.append(remaining_labels[0])
    if labels is not None:
        return np.array(uncorrelated_data).T, uncorrelated_labels
    else:
        return np.array(uncorrelated_data).T


def split_data_by_max_min(x, y, tf):

    """
    Wrapper for get_max_min_idxs
    Returns x and y split between training and testing based on tf.

    Parameters
    ----------
    x : 2D array
    y : 1D array
    tf : float
        The fraction of x and y assigned to training.

    """


    p_idxs = get_max_min_idxs(x, tf)
    training_idxs = [i for i in range(len(x)) if i in p_idxs]
    testing_idxs = [i for i in range(len(x)) if i not in p_idxs]
    training_x = [x[i] for i in training_idxs]
    training_y = [y[i] for i in training_idxs]
    testing_x = [x[i] for i in testing_idxs]
    testing_y = [y[i] for i in testing_idxs]

    return np.asarray(training_x), np.asarray(training_y), \
           np.asarray(testing_x), np.asarray(testing_y)


def get_max_min_idxs(data_x, tf, p_idxs=None, distances=None):
    
    assert tf<1.0 and tf>0., "tf must be between 0 and 1"


    """
    Identifies indices to split data_x using the max min algorithm
    Based on "Computer Aided Design of Experiments, Kennard R.W and Stone L.A (2012)"
    If p_idxs is given, this list is used as a starting point, else takes the two 
    furthest points. All distances calculated using L1.

    Parameters
    ----------
    data_x : 2D array
    tf : float
        The fraction of p_idxs to calculate.
    p_idxs : list of ints, optional
        If p_idxs is not None, p_idxs is used as a starting point.
    distances : 2D array, optional
        If distances is not None, distances is used as the distance matrix to
        calculate the p_idxs.

    Returns
    -------
    p_idxs : list of ints
        The idxs of data that should be assigned to training data.

    """

    def get_distance_arrays(data):
        return euclidean_distances(data, data)

    def get_initial_points(distances):
        sorted_distances = np.sort(distances)
        sorted_distance_idxs = np.argsort(distances)
        i_idx = sorted_distances[:,-1].argmax()
        j_idx = sorted_distance_idxs[i_idx][-1]
        return i_idx, j_idx

    def get_next_point_max_min(p_idxs, distances):
        min_distances = []
        for i in range(len(distances)):
            if i not in p_idxs:
                min_D = -1
                min_D_idx = -1
                for j in p_idxs:
                    if distances[i,j] < min_D or min_D == -1:
                        min_D = distances[i,j]
                        min_D_idx = i
                if min_D != -1 and min_D_idx != -1:
                    min_distances.append([min_D_idx,min_D])
        min_distances = sorted(min_distances, key=lambda x:x[1], reverse=True)
        return min_distances[0][0]
    
    if distances is None:
        distances = get_distance_arrays(data_x)
    if p_idxs is None:
        idx1, idx2 = get_initial_points(distances)
        p_idxs = [idx1, idx2]

    while len(p_idxs) < ceil(len(data_x)*tf):
        idx = get_next_point_max_min(p_idxs, distances)
        p_idxs.append(idx)

    return p_idxs


