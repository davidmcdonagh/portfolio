from cspy.alpha.crystal import CrystalStructure
import numpy as np

def get_mean_squared_error(predicted, reference):
    assert len(predicted) == len(reference),\
    "predicted and reference must have the same length."

    return sum([np.square(i - reference[count]) \
                          for count, i in enumerate(predicted)])/float(len(predicted))

def get_rmse(predicted, reference):
    return np.sqrt(get_mean_squared_error(predicted, reference))

def get_mean_signed_error(predicted, reference):
    assert len(predicted) == len(reference),\
    "predicted and reference must have the same length."
    return sum([i - reference[count] for count,i in enumerate(predicted)])

