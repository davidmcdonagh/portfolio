import unittest
import numpy as np

class TheanoNNUtils(unittest.TestCase):

    def test_network_structure(self):
        from code.ml_models.theano_nn import TheanoNN
        x = np.array(([9,2,1], [2,1,6]))
        y = np.array(([[1],[1]]))
        structure=[3,10,5,1]
        nn = TheanoNN(structure, x, y, x, y)
        self.assertEqual(structure,nn.structure)
        self.assertEqual(3,len(nn.weight_list))
        self.assertEqual(3,len(nn.bias_list))

        




if __name__ == '__main__':
    unittest.main()

