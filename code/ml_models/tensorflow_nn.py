import tensorflow as tf
import numpy as np

class TensorFlowNN:
    
    def __init__(self,structure, training_x, training_y, testing_x, testing_y, 
                 save_file, learning_rate = 0.0001, num_epochs=5000, num_batches=50):
        
        """
        Class for a basic tensor flow neural network.
        Parameters
        ----------
        structure : list of ints
            The structure of the network.
            E.g [5,10,1] for a single layer network with 5 input
            nodes, 10 nodes in the hidden layer, and a single output
            node; [5,10,8,1] for two hidden layers of size 10 and 8,
            respectively etc.
        training_input : n x m numpy array
            n is the number of training datapoints, m is the
            number of features of the data.
        training_output : n x m numpy array
            n is the number of training datapoints, m is the
            number of output features of the data.
        testing_input : n x m numpy array
            n is the number of testing datapoints, m is the
            number of features of the data.
        testing_output : n x m numpy array
            n is the number of testing datapoints, m is the
            number of output features of the data.
        learning_rate : float
            The stepsize for updating the weights of the
            network.
        save_file : string
            Where the trained structure will be saved.
        num_epochs : int
            The number of training iterations.
        num_batches : int
            The number of batches the training data is partitioned

        """

        
        self.structure = structure
        self.training_x = training_x
        self.training_y = training_y
        self.testing_x = testing_x
        self.testing_y = testing_y
        self.learning_rate = learning_rate
        self.num_epochs = num_epochs
        self.num_batches = num_batches
        self.total_training_error = []
        self.total_testing_error = []
        
        self.X = None
        self.y = None
        self.nn = None
        self.opt_f = None
        self.loss = None
        
        self.init_symbolic_variables()
        self.create_network()
        self.save_file = save_file
        self.saver=None
        
    def init_symbolic_variables(self):
        
        """
        Initialises the symbolic variables for the
        input and output nodes
        """
        
        self.X = tf.placeholder(tf.float32, shape=(None, self.structure[0]), name='X')
        self.y = tf.placeholder(tf.float32, shape=(None), name="y")

    def create_network(self):
        
        """
        Creates a Tensor Flow graph based on self.structure,
        sets the loss to mean squared error, and the optimiser
        to gradient descient.
        
        """
        
        def create_neuron_layer(X, n_neurons, name, activation=None):
            with tf.name_scope(name):
                n_inputs = int(X.get_shape()[1])
                stddev = 2 / np.sqrt(n_inputs + n_neurons)
                init = tf.truncated_normal((n_inputs, n_neurons), stddev=stddev)
                W = tf.Variable(init, name='kernel')
                b = tf.Variable(tf.zeros([n_neurons]), name='bias')
                Z = tf.matmul(X,W) + b
                if activation is not None:
                    return activation(Z)
                return Z

        with tf.name_scope('dnn'):
            self.nn = create_neuron_layer(self.X, self.structure[1], 
                                    name='hidden1', activation=tf.nn.relu)
            for i in range(1, len(self.structure)-1):
                if i == len(self.structure)-2:
                    self.nn = create_neuron_layer(self.nn, self.structure[i+1], 
                                            activation=None, name='output')
                else:
                    self.nn = create_neuron_layer(self.nn, self.structure[i+1], 
                                            activation=tf.nn.relu, name='hidden{}'.format(i+1))

        with tf.name_scope('loss'):
            self.loss = tf.losses.mean_squared_error(self.y, self.nn)

        with tf.name_scope('train'):
            optimizer = tf.train.GradientDescentOptimizer(self.learning_rate)
            self.opt_f = optimizer.minimize(self.loss)

        
    def fit(self, num_epochs=None):
        
        """
        Trains the neural network and saves the optimised
        graph to self.save_file.
        
        Parameters
        ----------
        num_epochs : int, optional
            If num_epochs is not None, the network is trained
            for num_epochs, else the network is trained for
            self.num_epochs.
        """
        
        if num_epochs is None:
            num_epochs = self.num_epochs
        self.saver = tf.train.Saver()
        init = tf.global_variables_initializer()
        with tf.Session() as sess:
            init.run()
            for epoch in range(num_epochs):
                sess.run(self.opt_f,feed_dict={self.X:self.training_x, self.y:self.training_y})
                cost_train = self.loss.eval(feed_dict={self.X:self.training_x, self.y:self.training_y})
                cost_test = self.loss.eval(feed_dict={self.X:self.testing_x, self.y:self.testing_y})

                print("{} Training loss:{} Testing loss:{}".format(epoch, cost_train,cost_test))
                self.total_training_error.append(cost_train)
                self.total_testing_error.append(cost_test)
                
            save_path = self.saver.save(sess, self.save_file)
        
    def predict(self, testing_x=None):
        
        """
        Calculations prediction values using model 
        stored at self.save_file.
        
        Parameters
        ----------
        testing_x : 2D array, optional
            If testing_x is not None, the network
            predicts values for testing_x, else the network
            predicts for self.testing_x.
            
        Returns
        -------
        y_pred : 2D array
            Predicted values.
        """
        
        if testing_x is None:
            testing_x = self.testing_x
        with tf.Session() as sess:
            self.saver.restore(sess, self.save_file)
            y_pred = self.nn.eval(feed_dict={self.X : testing_x})
            
        return y_pred

