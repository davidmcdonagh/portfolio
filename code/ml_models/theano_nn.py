import theano
import numpy as np
import matplotlib.pyplot as plt
import sys

class TheanoNN():
    
    def __init__(self, structure, training_input, training_output, 
                 testing_input, testing_output, num_epochs=5000, 
                 learning_rate=0.0001, num_batches=2, l2_lambda=.1):

        """
        Class to run a basic neural network in Theano using L2 regularisaton,
        mean-squared error cost function, and a relu nonlinear function.
        
        Parameters
        ----------
        structure : list of ints
            The structure of the network.
            E.g [5,10,1] for a single layer network with 5 input
            nodes, 10 nodes in the hidden layer, and a single output 
            node; [5,10,8,1] for two hidden layers of size 10 and 8,
            respectively etc.
        training_input : n x m numpy array
            n is the number of training datapoints, m is the
            number of features of the data.
        training_output : n x m numpy array
            n is the number of training datapoints, m is the
            number of output features of the data.
        testing_input : n x m numpy array
            n is the number of testing datapoints, m is the
            number of features of the data.
        testing_output : n x m numpy array
            n is the number of testing datapoints, m is the
            number of output features of the data.
        num_epochs : int
            The number of training iterations.
        learning_rate : float
            The stepsize for updating the weights of the
            network.
        num_batches : int
            The number of batches the training data is partitioned
            into for stochastic gradient descent.
        l2_lambda : float
            The coefficient to control L2 regularisation
        """
        
        self.structure = structure
        self.training_output = training_output
        self.testing_input = testing_input
        self.training_input = training_input
        self.testing_output = testing_output
        self.num_epochs = num_epochs
        self.learning_rate = learning_rate
        self.num_batches = num_batches
        self.l2_lambda = l2_lambda
        self.training_input_batches = []
        self.training_output_batches = []
        

        self.weight_list = []
        self.bias_list = []
        self.params = None

        self.total_training_error = []
        self.total_testing_error = []

        self.energy_calc = None
        self.gradients = None
        self.updates = []
        self.cost = None
        self.f_train = None
        self.f_test = None
        self.f_grad = None

        self.create_network()

    def create_network(self):
        
        """
        Wrapper function to call all functions required
        to create a neural network instance.
        """
        
        self.init_weight_list()
        self.init_symbolic_variables()
        self.init_symbolic_functions()
        
    def init_weight_list(self):
        
        """
        Initialises the weight lists with random values
        between -1 and 1, based on self.structure.
        
        Updates
        -------
        self.weight_list : list of list of floats
            The weights connecting nodes in the network
        self.bias_list : list of flots
            The bias nodes in the network.
        """
        
        for i in range(len(self.structure)-1):
            init_range = 2/np.sqrt(self.structure[i]+self.structure[i+1])
            self.weight_list.append(theano.shared(
                np.random.uniform(low=-init_range, high=init_range, 
                                  size=(self.structure[i],
                                        self.structure[i+1])), 
                                        "W" + str(i+1)))
            self.bias_list.append(theano.shared(1.))

    def init_symbolic_variables(self):
        
        """
        Initialises the symbolic variables for the
        input and output nodes, and the learning rate 
        of the network.
        
        Updates
        -------
        self.input : symbolic tensor matrix
        self.output : symbolic tensor matrix
        self.learning_rate : float
        
        """
        
        self.input = theano.tensor.matrix('input')
        self.output = theano.tensor.matrix('output')
        self.learning_rate = theano.shared(self.learning_rate)
        self.l2_lambda = theano.shared(self.l2_lambda)


    def init_symbolic_functions(self):
        
        """
        Initialises the symbolic functions that represent
        the neural network, by mapping self.input to self.output
        via self.weight_list and the non-linear mapping functions.
        
        Updates
        -------
        self.energy_calc : symbolic function
            The relationship between self.input, self.output and 
            self.weight_list to calculate the output of the network.
        self.cost : symbolic function
            The current cost (error) of the network.
        self.f_train : function
            The function used to iterate a training epoch of the network.
        self.f_test : function
            The function used to predict output values for test data.
        self.f_grad : function
            The gradients of all parameters of the network with respect
            to the cost function (useful for debugging).
        """
        
        self.energy_calc = theano.tensor.dot(self.input, self.weight_list[0]) + self.bias_list[0]
        for i in range(1,len(self.weight_list)):
            self.energy_calc = theano.tensor.dot(theano.tensor.nnet.relu(self.energy_calc), 
                                             self.weight_list[i]) + self.bias_list[i]
        
        self.cost = np.square(self.energy_calc-self.output).sum() * (1/float(len(self.training_input)))\
                    + self.l2_lambda*.5*sum([i.sum() for i in self.weight_list])
        params = []
        for i in self.weight_list:
            params.append(i)
        for i in self.bias_list:
            params.append(i)
        self.params = params
        self.gradients = theano.tensor.grad(self.cost, params)
        for count, i in enumerate(params):
            update = i - (self.learning_rate * self.gradients[count])
            self.updates.append([i, update])
        self.f_train = theano.function([self.input, self.output], 
                                       self.energy_calc, updates=self.updates, 
                                       allow_input_downcast=True)
        self.f_test = theano.function([self.input], self.energy_calc, allow_input_downcast=True)
        self.f_grad = ([params], self.gradients)
        
    def partition_training_data(self):
        
        """
        Shuffles and partitions the training data
        for stochastic gradient descent.
        
        Updates
        -------
        self.training_input_batches : list of numpy arrays
        self.training_output_batches : list of numpy arrays
        
        Notes
        -----
        If the size of the training data does not divide exactly
        by self.num_batches, the final batch will include the 
        remainder.
        """
        
        rng_state = np.random.get_state()
        np.random.shuffle(self.training_input)
        np.random.set_state(rng_state)
        np.random.shuffle(self.training_output)
        
        batch_size = int(len(self.training_input)/self.num_batches)
        self.training_input_batches = []
        self.training_output_batches = []
        
        for i in range(self.num_batches):
            if i == self.num_batches - 1:
                input_batch = self.training_input[i*batch_size:]
                self.training_input_batches.append(input_batch)
                output_batch = self.training_output[i*batch_size:]
                self.training_output_batches.append(output_batch)
            else:
                input_batch = self.training_input[i*batch_size:(i+1)*batch_size]
                self.training_input_batches.append(input_batch)
                output_batch = self.training_output[i*batch_size:(i+1)*batch_size]
                self.training_output_batches.append(output_batch)
            
    def fit(self):
        
        """
        For each epoch, shuffles the training data into
        self.num_batches, and updates the weights of the network
        using self.f_train.
        After training the error and correlation are plotted.
        
        Updates
        -------
        self.weight_list : list of list of floats
        self.bias_list : list of floats
        self.total_training_error : list of floats
        self.total_testing_error : list of floats
        
        """
        
        for i in range(self.num_epochs):
            self.partition_training_data()
            for count, batch in enumerate(self.training_input_batches):
                self.f_train(batch, self.training_output_batches[count])

            training_error = (np.square(self.f_test(self.training_input)
                                    - self.training_output).sum()
                                     * (1/float(len(self.training_input))))
            testing_error = (np.square((self.f_test(self.testing_input)
                                    - self.testing_output)).sum())* (1/float(len(self.testing_input)))
            if i % 10 == 0:
                sys.stdout.write('\r')
                sys.stdout.write(" Training error: " + str(training_error) + " kJ/mol Testing error: " +
                                 str(testing_error) + " kJ/mol Training complete: "+ str(int(round((i + 1) /
                                                                    float(self.num_epochs) * 100))) + "%")
                sys.stdout.flush()
            self.total_training_error.append(training_error)
            self.total_testing_error.append(testing_error)


    def predict(self, x=None):

        """
        Calculates prediction values using self.f_test.
        
        Parameters
        ----------
        x : 2D array, optional
            If x is not None, predictions are made for x,
            else predictions are made for self.testing_input.
            
        Returns
        -------
        predicted_vals : 1D array
            
        """

        if x is None:
            return self.f_test(self.testing_input)
        else:
            return self.f_test(x)


