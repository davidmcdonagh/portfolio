from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, WhiteKernel, Matern
import pickle

class GaussianProcess:
    
    def __init__(self, kernel_name, training_x, training_y,
              testing_x, testing_y, kernel_args={}, alpha=1E-10,
                 noise_kernel_args=None):
    
        """
        Wrapper class for the sklearn GaussianProcessRegressor class.

        Parameters
        ----------
        kernel_name : string
            matern and RBF are current supported.
        training_x : 2D array
        training_y : 1D array
        testing_x : 2D array
        testing_y : 1D array
        kernel_args : dict
            Optional arguments for creating the kernel.
            For RBF: {'length_scale', 'length_scale_bounds'}
            for Matern : {'length_scale', 'length_scale_bounds', 'nu'
        alpha : float
            Value added to the diagonal of the kernel matrix during fitting.
        noise_kernel_args : dict, optional
            If not none, a noise kernel is added to the kernel
            {'noise_level', 'noise_level_bounds'}
        """

        self.kernel_name = kernel_name
        self.alpha = alpha
        self.training_x = training_x
        self.training_y = training_y
        self.testing_x = testing_x
        self.testing_y = testing_y
        self.kernel_args = kernel_args
        self.noise_kernel_args = noise_kernel_args
        self.gp = self.setup_gp()

    def setup_kernel(self):
        
        """
        Creates a kernel based on self.kernel_name,
        self.kernel_args, and self.noise_kernel_args
        
        Returns
        -------
        kernel : Kernel
        """
        
        kernel_dict = {"matern" : Matern, "RBF" : RBF}
        assert self.kernel_name in kernel_dict, \
        "kernel {} not supported. Must be one of {}".format(self.kernel_name, kernel_dict.keys())
        kernel = kernel_dict[self.kernel_name](**self.kernel_args)
        if self.noise_kernel_args is not None:
            kernel += WhiteKernel(**self.noise_kernel_args)

        return kernel

    
    def setup_gp(self):
        
        """
        Creates a GaussianProcessRegressor and assigns to
        self.gp.
        """

        return GaussianProcessRegressor(kernel=self.setup_kernel(), alpha=self.alpha)
    
    
    def fit(self,x=None,y=None):
        
        """
        Fits the self.gp model.
        
        Parameters
        ----------
        x : 2D array, optional
        y : 1D array, optional
            If x and y are not None, self.gp is trained
            using x and y.
            Else self.gp is trained using self.training_x
            and self.training_y.
        
        Updates
        -------
        self.gp
        """
        
        if x is not None and y is not None:
            assert(len(x) == len(y)), \
            "x and y must have the same length."
            self.gp.fit(x,y)
        else:
            self.gp.fit(self.training_x, self.training_y)
        
    def predict(self, x=None, return_std=False):
        
        """
        Calculates prediction values using self.gp.
        
        Parameters
        ----------
        x : 2D array, optional
            If x is not None, predictions are made for x,
            else predictions are made for self.testing_x.
        return_std: boolean, optional
            if True, the std for each prediction are also returned
            
        Returns
        -------
        predicted_vals : 1D array
        If return_std:
            predicted_vals : 1D array
            std_vals : 1D array
            
        """
        
        if x is None:
            return self.gp.predict(self.testing_x, return_std=return_std)
        else:
            return self.gp.predict(x, return_std, return_std)
        
    def save_model(self, path):
        
        """
        Saves self.gp to path as a pickle file.
        """
        
        with open(path, "wb") as g:
            pickle.dump(self.gp, g, protocol=pickle.HIGHEST_PROTOCOL)
            
    def load_model(self, path):
        
        """
        Loads pickle file at path and assigns to self.gp.
        """
        
        with open(path, "rb") as pf:
            self.gp = pickle.load(pf)
        
